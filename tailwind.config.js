const colors = require('tailwindcss/colors')

module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {},
    colors: {
      transaction: '#101010',
      'asset': '#111517',
      'asset-selected': '#19425B',
      'border': '#2274a5',
      button: '#FEC004',
      ...colors
    }
  },
  plugins: [],
}
