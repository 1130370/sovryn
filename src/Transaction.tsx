import { FC, useMemo, useState } from 'react';
import * as ethers from 'ethers';
import cx from 'classnames';

import eth from './eth.svg';
import weenus from './weenus.svg';

interface Props {
  ethBalance?: ethers.BigNumber;
  weenusBalance?: ethers.BigNumber;
  onSubmitTransaction: (to: string, amount: string, currency: string) => void;
}

const ZERO = ethers.BigNumber.from(0);
const format = ethers.utils.formatEther;

const Transaction: FC<Props> = ({
  ethBalance = ZERO,
  weenusBalance = ZERO,
  onSubmitTransaction,
}) => {
  const [amount, setAmount] = useState<string>('');
  const [recipient, setRecipient] = useState('');
  const [isEth, setIsEth] = useState(true);

  const balance = isEth ? ethBalance : weenusBalance;

  const isValid = useMemo(() => {
    const isAddress = ethers.utils.isAddress(recipient);

    if (!isAddress) return false;

    try {
      const validAmount = ethers.utils.parseEther(amount);

      return validAmount.gt(0) && validAmount.lte(balance);
    } catch {
      return false;
    }
  }, [amount, recipient, balance]);

  return (
    <div className="flex flex-col" style={{ width: 400 }}>
      <h1 className="text-center text-4xl font-bold mb-8">SEND</h1>
      <div className="flex flex-col mb-9">
        <div className="text-xl mb-2">Asset:</div>
        <div className="inline-flex mb-1">
          <button
            onClick={() => setIsEth(true)}
            className={cx(
              isEth ? 'bg-asset-selected' : 'bg-asset',
              'hover:bg-gray-400 text-white font-bold py-2 px-2 rounded-l flex flex-row items-center justify-center flex-1 border border-border'
            )}
          >
            <img src={eth} alt="eth" className="inline mr-2" />
            rETH
          </button>
          <button
            onClick={() => setIsEth(false)}
            className={cx(
              !isEth ? 'bg-asset-selected' : 'bg-asset',
              'hover:bg-gray-400 text-white font-bold py-2 px-2 rounded-r flex flex-row items-center justify-center flex-1 border border-border'
            )}
          >
            <img src={weenus} alt="weenus" className="inline mr-2" />
            WEENUS
          </button>
        </div>
        <span className="text-xs">
          Available Balance: {format(balance)} {isEth ? 'rETH' : 'WEENUS'}
        </span>
      </div>
      <div className="flex flex-col mb-9">
        <div className="text-xl mb-2">Amount:</div>
        <div className="flex flex-grow-1 text-black px-4 py-2 mb-2 bg-white rounded">
          <input
            className="appearance-none outline-none border-none flex flex-1 text-center font-bold text-lg"
            type="text"
            value={amount}
            onChange={(e) => setAmount(e.target.value)}
          />
          <div className="flex-shrink-1 bg-white select-none font-bold text-lg">
            {isEth ? 'rETH' : 'WEENUS'}
          </div>
        </div>
        <div className="inline-flex mb-1 text-asset-selected">
          <button
            onClick={() => setAmount(format(balance.div(10)))}
            className="hover:bg-gray-400 font-bold py-2 px-4 rounded-l flex-1 border border-border"
          >
            10%
          </button>
          <button
            onClick={() => setAmount(format(balance.div(4)))}
            className="hover:bg-gray-400 font-bold py-2 px-4 flex-1 border border-border"
          >
            25%
          </button>
          <button
            onClick={() => setAmount(format(balance.div(2)))}
            className="hover:bg-gray-400 font-bold py-2 px-4 flex-1 border border-border"
          >
            50%
          </button>
          <button
            onClick={() =>
              setAmount(format(balance.div(2).add(balance.div(4))))
            }
            className="hover:bg-gray-400 font-bold py-2 px-4 flex-1 border border-border"
          >
            75%
          </button>
          <button
            onClick={() => setAmount(format(balance))}
            className="hover:bg-gray-400 font-bold py-2 px-4 rounded-r flex-1 border border-border"
          >
            100%
          </button>
        </div>
      </div>
      <div className="flex flex-col mb-9">
        <div className="text-xl mb-2">Send To:</div>
        <div className="flex flex-grow-1 text-black px-4 py-2 mb-2 bg-white rounded">
          <input
            className="appearance-none outline-none border-none flex flex-1 text-center font-bold text-lg"
            type="text"
            value={recipient}
            onChange={(e) => setRecipient(e.target.value)}
            placeholder="Type or Paste address"
          />
        </div>
      </div>
      <button
        disabled={!isValid}
        className={cx(
          !isValid && 'opacity-25',
          'bg-button p-4 mx-16 rounded text-black font-bold text-xl'
        )}
        onClick={() =>
          onSubmitTransaction(recipient, amount, isEth ? 'rETH' : 'WEENUS')
        }
      >
        SUBMIT
      </button>
    </div>
  );
};

export default Transaction;
