export interface TxInfo {
  from: string;
  to: string;
  amount: string;
  currency: string;
}
