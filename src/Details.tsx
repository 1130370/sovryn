import { FC } from 'react';

import check from './check.png';

interface Props {
  hash?: string;
  confirmed?: boolean;
  onClose: () => void;
}

const Details: FC<Props> = ({ hash, confirmed, onClose }) => {
  const status = confirmed ? 'Successful' : !hash ? 'Sending' : 'Processing';

  return (
    <div className="flex flex-col" style={{ width: 400 }}>
      <h1 className="text-center text-2xl font-bold mb-8">
        Transaction Details
      </h1>
      <div className="flex flex-col mb-9 text-center items-center">
        {status !== 'Sending' && <img src={check} width={52} height={52} />}
        <div className="text-2xl mb-1">Status: {status}</div>
      </div>
      {hash && (
        <div className="mb-9 text-center">
          Tx Hash:{' '}
          <a
            className="text-button"
            href={`https://ropsten.etherscan.io/tx/${hash}`}
          >
            {hash.slice(0, 10)}...{hash.slice(-10)}
          </a>
        </div>
      )}
      <button
        className="bg-button p-4 mx-16 rounded text-black font-bold text-xl"
        onClick={onClose}
      >
        Close
      </button>
    </div>
  );
};

export default Details;
