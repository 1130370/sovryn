import { FC, useEffect, useState } from 'react';
import * as ethers from 'ethers';
import cx from 'classnames';

interface Props {
  provider?: ethers.providers.Web3Provider;
  connectWallet: (account: ethers.providers.Web3Provider) => void;
}

const Header: FC<Props> = ({ provider, connectWallet }) => {
  const handleConnect = async () => {
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    await provider.send('eth_requestAccounts', []);
    connectWallet(provider);
  };

  const [address, setAddress] = useState<string>();

  useEffect(() => {
    if (provider) {
      provider?.getSigner().getAddress().then(setAddress);
    }
  }, [provider]);

  return (
    <div className="flex max-w-screen justify-center bg-black">
      <div className="flex flex-1 justify-center max-w-7xl">
        <div className="flex flex-1 items-center justify-between h-16">
          <div className="flex items-center">
            <div className="flex-shrink-0">
              <img
                className="h-8 w-8"
                src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
                alt="Workflow"
              />
            </div>
          </div>
          <button
            className={cx(
              provider ? 'bg-asset-selected' : 'bg-asset',
              'hover:bg-gray-400 text-white font-bold py-2 px-4 rounded border border-border'
            )}
            onClick={handleConnect}
          >
            {provider
              ? `${address?.slice(0, 4)}...${address?.slice(-4)}`
              : 'Connect Wallet'}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Header;
