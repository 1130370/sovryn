import * as ethers from 'ethers';
import { useEffect, useState } from 'react';
import Header from './Header';
import Transaction from './Transaction';
import Review from './Review';
import Details from './Details';
import { TxInfo } from './types';
import weenusAbi from './weenusAbi.json';

interface TxState {
  hash?: string;
  confirmed?: boolean;
}

type Stage = 'input' | 'confirm' | 'sent';

const WEENUS_ADDRESS = '0x101848D5C5bBca18E6b4431eEdF6B95E9ADF82FA';
const WEENUS = new ethers.Contract(WEENUS_ADDRESS, weenusAbi);

function App() {
  const [stage, setStage] = useState<Stage>('input');
  const [txInfo, setTxInfo] = useState<TxInfo>();
  const [txState, setTxState] = useState<TxState>({});
  const [provider, setProvider] = useState<ethers.providers.Web3Provider>();
  const [ethBalance, setEthBalance] = useState<ethers.BigNumber>();
  const [weenusBalance, setWeenusBalance] = useState<ethers.BigNumber>();

  const updateBalance = async () => {
    if (!provider) {
      return;
    }

    const address = await provider.getSigner().getAddress();

    provider.getBalance(address).then(setEthBalance);
    WEENUS.connect(provider.getSigner())
      .balanceOf(address)
      .then(setWeenusBalance);
  };

  const submitTransaction = async (
    to: string,
    amount: string,
    currency: string
  ) => {
    const address = await provider?.getSigner().getAddress();

    if (!address) {
      return; // Ideally show some error feedback here
    }

    setTxInfo({
      to,
      amount,
      currency,
      from: address,
    });
    setStage('confirm');
  };

  const sendTransaction = async () => {
    if (!txInfo || !provider) {
      return;
    }

    setStage('sent');
    const value = ethers.utils.parseEther(txInfo.amount);

    let sentTx: ethers.providers.TransactionResponse;

    if (txInfo.currency === 'WEENUS') {
      sentTx = await WEENUS.connect(provider?.getSigner()).transfer(
        txInfo.to,
        value
      );
    } else {
      const tx = {
        to: txInfo.to,
        value,
      };

      sentTx = await provider?.getSigner().sendTransaction(tx);
    }

    if (sentTx) {
      sentTx.wait().then(() => {
        setTxState((currentState) => {
          // Ensure we're still looking at the same transaction
          if (currentState.hash === sentTx.hash) {
            return { hash: currentState.hash, confirmed: true };
          }

          return currentState;
        });
      });

      setTxState({ hash: sentTx.hash });
    } else {
      setStage('input');
    }
  };

  useEffect(() => {
    if (provider) {
      updateBalance();
      provider.on('block', updateBalance);

      return () => {
        provider.off('block', updateBalance);
      };
    }
  }, [provider]);

  return (
    <div className="min-h-screen max-h-screen bg-neutral-900">
      <Header provider={provider} connectWallet={setProvider} />
      <div className="flex flex-1 items-center justify-center">
        <div className="px-10 py-7 my-16 rounded-2xl bg-transaction text-white">
          {stage === 'input' && (
            <Transaction
              ethBalance={ethBalance}
              weenusBalance={weenusBalance}
              onSubmitTransaction={submitTransaction}
            />
          )}
          {stage === 'confirm' && (
            <Review {...txInfo} onConfirmTransaction={sendTransaction} />
          )}
          {stage === 'sent' && (
            <Details
              {...txState}
              onClose={() => {
                setStage('input');
                setTxState({});
              }}
            />
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
