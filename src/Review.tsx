import { FC, useEffect, useState } from 'react';
import * as ethers from 'ethers';

import arrow from './arrow.png';

interface Props {
  from?: string;
  to?: string;
  amount?: string;
  currency?: string;
  onConfirmTransaction: () => void;
}

const Transaction: FC<Props> = ({
  from,
  to,
  amount,
  currency,
  onConfirmTransaction,
}) => {
  const isValid = from && to && amount && currency;
  const [fee, setFee] = useState('');

  useEffect(() => {
    if (!to || !amount) {
      return;
    }

    ethers
      .getDefaultProvider()
      .estimateGas({
        to,
        value: ethers.utils.parseEther(amount),
      })
      .then((estimatedFee) => setFee(ethers.utils.formatEther(estimatedFee)));
  }, [to, amount]);

  if (!isValid) {
    return <div>Invalid transaction, how did you get here?</div>;
  }

  return (
    <div className="flex flex-col" style={{ width: 400 }}>
      <h1 className="text-center text-2xl font-bold mb-8">
        Review Transaction
      </h1>
      <div className="flex flex-col mb-9 text-center">
        <div className="text-2xl mb-1">SEND</div>
        <div className="text-2xl mb-2">
          {ethers.utils.commify(amount)} {currency}
        </div>
      </div>
      <div className="flex flex-col mb-9">
        <div>From: {from}</div>
        <img
          src={arrow}
          alt="from-to"
          className="my-4 mx-auto"
          style={{ width: 30 }}
        />
        <div>To: {to}</div>
      </div>
      <div className="mb-9">
        Tx Fee: {fee ? `${fee} rETH` : 'Estimating...'}
      </div>
      <button
        className="bg-button p-4 mx-16 rounded text-black font-bold text-xl"
        onClick={onConfirmTransaction}
      >
        Confirm
      </button>
    </div>
  );
};

export default Transaction;
